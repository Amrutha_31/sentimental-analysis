# Sentimental Analysis

Sentiment analysis is the task of identifying whether the opinion expressed in a text is positive or negative in general. It is a kind of data mining where you measure the inclination of people's opinions by using NLP.

This project analyses everyday language that we used in communication and produces number of tokens in the language,keywords,polarity and subjectivity of the context.
